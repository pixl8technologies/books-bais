<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Books Categories</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Books Categories</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-5">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Category -->
                <?php
                if (isset($_POST['add_category'])) {
                  $error = "";
                  $sql = "INSERT INTO category (name) VALUES ('" . $_POST['add_category'] . "')";
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Book category <strong><?php echo $_POST['add_category']; ?></strong> has been added.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Edit Category -->
                <?php
                if (isset($_POST['edit_category'])) {
                  $error = "";
                  $sql = "UPDATE category SET name='" . $_POST['edit_category_name'] . "' WHERE id=" . $_POST['edit_category'];
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Book category <strong><?php echo $_POST['edit_category_name']; ?></strong> (ID = <?php echo $_POST['edit_category']; ?>) data has been updated.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Delete Category -->
                <?php
                if (isset($_POST['delete_category'])) {
                  $sql = "DELETE FROM category WHERE id=" . $_POST['delete_category'];
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Category <strong><?php echo $_POST['delete_category_name']; ?></strong> (ID = <?php echo $_POST['delete_category']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4>Categories</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="category-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10 text-center">ID</th>
                      <th class="py-10 text-center">Name</th>
                      <th class="py-10 text-center" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($conn, "SELECT * FROM category");
                    if (mysqli_num_rows($result) > 0) { while($row_categories = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_categories['id'];?></td>
                      <td data-toggle="modal" data-target="#category-edit-modal-<?php echo $row_categories['id'];?>" class="cursor-pointer py-10 px-15 text-left"><?php echo $row_categories['name'];?></td>
                      <td>
                        <a data-toggle="modal" data-target="#category-edit-modal-<?php echo $row_categories['id'];?>" href="#"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="category-edit-modal-<?php echo $row_categories['id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Category</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-8 form-group">
                                    <input type="text" class="d-none" name="edit_category" value="<?php echo $row_categories['id'];?>">
                                    <input type="text" class="d-none" name="edit_category_name_old" value="<?php echo $row_categories['name'];?>">
                                    <input type="text" class="form-control" name="edit_category_name" placeholder="<?php echo $row_categories['name'];?>" value="<?php echo $row_categories['name'];?>">
                                  </div>
                                  <div class="col-md-4 text-right">
                                    <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_category" value="<?php echo $row_categories['id'];?>">
                          <input type="text" class="d-none" name="delete_category_name" value="<?php echo $row_categories['name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

                <div class="text-right mt-30">
                  <form action="" method="post">
                    <div class="input-group">
                      <input type="text" name="add_category" class="form-control py-20" placeholder="Category Name" required>
                      <button type="submit" class="btn btn-primary input-group-addon">Add Category</button>
                    </div>
                  </form>
                </div>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>