<?php
include("../config.php");
include("../db_connection.php");
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>Login | Books Borrowing and Inventory System</title>
    
    <link rel="apple-touch-icon" href="<?php echo $root_dir; ?>/remark/base/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/remark/base/assets/images/favicon.ico">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $root_dir; ?>/assets/favicon/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/assets/favicon/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $root_dir; ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $root_dir; ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $root_dir; ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $root_dir; ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/flag-icon-css/flag-icon.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/examples/css/pages/forgot-password.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition page-forgot-password layout-full">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
      <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        <h2>Books Borrowing and Inventory System</h2>
        <p>Input the admin password to open dashboard</p>

        <form method="post" action="../includes/login_script.php" role="form">
          <div class="form-group">
            <input type="text" class="form-control mb-10" id="inputUsername" name="username" placeholder="Your Username">
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Your Password">
          </div>
          <div class="form-group">
            <button type="submit" name = "submit" class="btn btn-primary btn-block">Log in</button>
          </div>
        </form>

        <footer class="page-copyright">
          <p>CREATED BY Pixl8 Technologies</p>
          <p>© 2018. All RIGHT RESERVED.</p>
          <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-twitter" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-facebook" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-dribbble" aria-hidden="true"></i>
        </a>
          </div>
        </footer>
      </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery/jquery.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/animsition/animsition.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/switchery/switchery.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/intro-js/intro.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Scripts -->
    <script src="<?php echo $root_dir; ?>/remark/global/js/Component.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Base.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Config.js"></script>
    
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/Menubar.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/GridMenu.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/Sidebar.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/PageAside.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Plugin/menu.js"></script>
    
    <script src="<?php echo $root_dir; ?>/remark/global/js/config/colors.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/config/tour.js"></script>
    <script>Config.set('assets', '<?php echo $root_dir; ?>/remark/base/assets');</script>
    
    <!-- Page -->
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Site.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/asscrollable.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/slidepanel.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/switchery.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-placeholder.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/material.js"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
  </body>
</html>