<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Health RMS Settings Panel</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Settings</li>
        </ol>
      </div>

      <div class="page-content">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-4">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Class -->
                <?php
                if (isset($_POST['add_class'])) {
                  $sql = "INSERT INTO class (`name`) VALUES ('";
                  $sql .= mysqli_real_escape_string( $conn , $_POST['add_class'] ) . "')";
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Class <strong><?php echo $_POST['add_class']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <!-- Edit Class -->
                <?php
                if (isset($_POST['edit_class'])) {
                  $sql = "UPDATE class SET name='" . $_POST['edit_name'] . "' WHERE id=" . $_POST['edit_class'] . "";
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Class <strong><?php echo $_POST['edit_name_old']; ?></strong> (ID = <?php echo $_POST['edit_class']; ?>) has been updated to <strong><?php echo $_POST['edit_name']; ?></strong>.
                  </div>
                <?php } } ?>

                <!-- Delete Class -->
                <?php
                if (isset($_POST['delete_class'])) {
                  $sql = "DELETE FROM class WHERE id=" . $_POST['delete_class'];
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo (isset($error) ? $error : '') . mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Class <strong><?php echo $_POST['delete_name']; ?></strong> (ID = <?php echo $_POST['delete_class']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4>Classes</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="class-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10 text-center">ID</th>
                      <th class="py-10 text-center">Name</th>
                      <th class="py-10 text-center" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($conn, "SELECT * FROM class ORDER BY name");
                    if (mysqli_num_rows($result) > 0) { while($row_classes = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_classes['id'];?></td>
                      <td class="py-10 px-15 text-left"><?php echo $row_classes['name'];?></td>
                      <td>
                        <a data-toggle="modal" data-target="#class-edit-modal-<?php echo $row_classes['id'];?>" href="#"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="class-edit-modal-<?php echo $row_classes['id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Class</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-8 form-group">
                                    <input type="text" class="d-none" name="edit_class" value="<?php echo $row_classes['id'];?>">
                                    <input type="text" class="d-none" name="edit_name_old" value="<?php echo $row_classes['name'];?>">
                                    <input type="text" class="form-control" name="edit_name" placeholder="<?php echo $row_classes['name'];?>" value="<?php echo $row_classes['name'];?>">
                                  </div>
                                  <div class="col-md-4 text-right">
                                    <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_class" value="<?php echo $row_classes['id'];?>">
                          <input type="text" class="d-none" name="delete_name" value="<?php echo $row_classes['name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

                <div class="text-right mt-30">
                  <form action="" method="post">
                    <div class="input-group">
                      <input type="text" name="add_class" class="form-control py-20" placeholder="Class Description">
                      <button type="submit" class="btn btn-primary input-group-addon">Add Class</button>
                    </div>
                  </form>
                </div>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>