<?php
if ( file_exists("../config.php") ) include("../config.php");
if ( file_exists("config.php") ) include("config.php");
if ( file_exists("../db_connection.php") ) include("../db_connection.php");
if ( file_exists("db_connection.php") ) include("db_connection.php");

if ($_GET['q'] === "") echo '';

else if ($_GET['by'] === 'book') {
    $sql = "SELECT * FROM book WHERE (title LIKE '%";
    $sql .= $_GET['q'] . "%') LIMIT 10";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        echo '
        <div class="example table-responsive">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Author</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>';
            while($row = mysqli_fetch_assoc($result)) {
                echo '
                <tr onclick="window.open(\'' . $root_dir . '/edit?id=' . $row['id'] . '\')" style="cursor: pointer;">
                <td>' . $row['id'] . '</td>
                <td>' . $row['title'] . '</td>
                <td>' . $row['author'] . '</td>
                <td><span class="badge badge-outline badge-default">On hand</span></td></a>
                </tr>';
            }
        echo '
            </tbody>
          </table>
        </div>';
    } else echo '<p class="text-center mt-20">No results.</p>'; mysqli_close($conn);
}

else if ($_GET['by'] === 'name') {
    $sql = "SELECT * FROM `student` WHERE (name_last LIKE '%";
    $sql .= $_GET['q'] . "%' OR name_first LIKE '%";
    $sql .= $_GET['q'] . "%' OR name_middle LIKE '%";
    $sql .= $_GET['q'] . "%') LIMIT 10";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        echo '
        <div class="example table-responsive">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Guardian</th>
                <th>Class</th>
              </tr>
            </thead>
            <tbody>';
            while($row = mysqli_fetch_assoc($result)) {
                echo '
                <tr onclick="window.open(\'' . $root_dir . '/edit?id=' . $row['id'] . '\')" style="cursor: pointer;">
                <td>' . $row['id'] . '</td>
                <td>' . $row['name_first'] . ' ' . $row['name_middle'] . ' ' . $row['name_last'] . '</td>
                <td>' . $row['guardian_name'] . '</td>
                <td>' . $row['gys'] . '</td></a>
                </tr>';
            }
        echo '
            </tbody>
          </table>
        </div>';
    } else echo '<p class="text-center mt-20">No results.</p>'; mysqli_close($conn);
}

else if ($_GET['by'] === 'student-number') {
    echo '<p class="text-center mt-20">Student Number not yet supported.</p>';
}

else if ($_GET['by'] === 'guardian') {
    $sql = "SELECT * FROM `student` WHERE guardian_name LIKE '%" . $_GET['q'] . "%' LIMIT 10";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        echo '
        <div class="example table-responsive">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Guardian</th>
                <th>Class</th>
              </tr>
            </thead>
            <tbody>';
            while($row = mysqli_fetch_assoc($result)) {
                echo '
                <tr onclick="window.open(\'' . $root_dir . '/edit?id=' . $row['id'] . '\')" style="cursor: pointer;">
                <td>' . $row['id'] . '</td>
                <td>' . $row['name_first'] . ' ' . $row['name_middle'] . ' ' . $row['name_last'] . '</td>
                <td>' . $row['guardian_name'] . '</td>
                <td>' . $row['gys'] . '</td></a>
                </tr>';
            }
        echo '
            </tbody>
          </table>
        </div>';
    } else echo '<p class="text-center mt-20">No results.</p>'; mysqli_close($conn);
}

else if ($_GET['by'] === 'class') {
    $sql = "SELECT * FROM `student` WHERE gys LIKE '%" . $_GET['q'] . "%' LIMIT 10";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        echo '
        <div class="example table-responsive">
          <table class="table table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Guardian</th>
                <th>Class</th>
              </tr>
            </thead>
            <tbody>';
            while($row = mysqli_fetch_assoc($result)) {
                echo '
                <tr onclick="window.open(\'' . $root_dir . '/edit?id=' . $row['id'] . '\')" style="cursor: pointer;">
                <td>' . $row['id'] . '</td>
                <td>' . $row['name_first'] . ' ' . $row['name_middle'] . ' ' . $row['name_last'] . '</td>
                <td>' . $row['guardian_name'] . '</td>
                <td>' . $row['gys'] . '</td></a>
                </tr>';
            }
        echo '
            </tbody>
          </table>
        </div>';
    } else echo '<p class="text-center mt-20">No results.</p>'; mysqli_close($conn);
}

else echo '';