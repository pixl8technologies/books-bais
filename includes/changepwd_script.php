<?php
session_start();
include_once '../config.php';
include_once '../db_connection.php';
if(isset($_SESSION['u_id'])){
    $userId = $_SESSION['u_id'];
    $userName = $_SESSION['username'];
    }else{
      header("Location: log-in/index.php?notloggedin");
        }

if(isset($_POST['submit'])){

    
    $pwd1 = mysqli_real_escape_string($conn, $_POST['password']);
    $pwd2 = mysqli_real_escape_string($conn, $_POST['password2']);
    

    if (empty($pwd1) || empty($pwd2)){
        //nooo
        header("Location: ../changepwd/index.php?empty");
        exit();

        } else {
        
            if($pwd1 != $pwd2){
                header("Location: index.php?passwordsnotequal");
            } else{
                $pwd = $pwd1;
                $sql = "SELECT password FROM user where username = '$userName'";
                $genQu = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($genQu);
                $hashedPwd = password_hash($pwd, PASSWORD_BCRYPT);
            
                $queryChange = mysqli_query($conn, "UPDATE user SET password = '$hashedPwd' WHERE username ='$userName'");
                
                header("Location: ../index.php"); 
            }
    }
} else {
    header("Location: ../log-in/index.php?submitbuttonfail");
    exit();
}



?>