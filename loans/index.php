<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Loans</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Loans</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-12">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Loan -->
                <?php
                if (isset($_POST['add_loan_title'])) {
                  $error = "";
                  $sql = "INSERT INTO loan (title, author) VALUES ('" . 
                    mysqli_real_escape_string( $conn , $_POST['add_loan_title'] ) . "', '" .
                    mysqli_real_escape_string( $conn , $_POST['add_loan_author'] ) . "')";
                  if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
                  // Add categories to loan
                  $sql = "";
                  foreach ($_POST['add_loan_category'] as $key => $value)
                    $sql .= "(" . mysqli_insert_id($conn) . ", " . $value . "), ";
                  if ($sql !== "") {
                    $sql = "INSERT INTO loan_category_link (loan_id, category_id) VALUES " . $sql;
                    $sql = substr( $sql, 0, (strlen($sql) - 2) );
                    if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
                  }
                  if ( $error !== "" ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo $error; ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Loan <strong><?php echo $_POST['add_loan_title']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <!-- Edit loan -->
                <?php
                if (isset($_POST['edit_loan'])) {
                  $error = "";
                  $sql = "UPDATE loan SET " .
                    "title='" . $_POST['edit_loan_title'] . "', " .
                    "author='" . $_POST['edit_loan_author'] . "' WHERE id=" . $_POST['edit_loan'];
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Loan <strong><?php echo $_POST['edit_loan_title']; ?></strong> (ID = <?php echo $_POST['edit_loan']; ?>) data has been updated.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Delete loan -->
                <?php
                if (isset($_POST['delete_loan'])) {
                  $error = "";
                  $sql = "DELETE FROM loan_category_link WHERE loan_id=" . $_POST['delete_loan'];
                  if ( !(mysqli_query($conn, $sql)) ) $error .= "";
                  $sql = "DELETE FROM loan WHERE id=" . $_POST['delete_loan'];
                  if ( !(mysqli_query($conn, $sql)) ) $error .= "";
                  if ( $error !== "" ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo $error; ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Loan <strong><?php echo $_POST['delete_loan_name']; ?></strong> (ID = <?php echo $_POST['delete_loan']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <div class="position-relative">
                  <h4 class="mb-20 pt-10">All Loans</h4>
                  <div class="position-absolute" style="right: 0; top: 0;">
                    <button data-toggle="modal" data-target="#add-loan-modal" class="btn btn-primary" type="button">Add a Loan</button>
                    <!-- Modal -->
                    <div class="modal fade modal-fade-in-scale-up" id="add-loan-modal" aria-hidden="false" role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Add a Loan</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group">
                              <input type="text" class="form-control" name="add_loan_title" placeholder="Title">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="add_loan_author" placeholder="Author">
                            </div>
                            <div class="form-group">
                              <select name="add_loan_category[]" class="form-control" sdata-plugin="select2">
                                <?php $result_categories = mysqli_query($conn, "SELECT * FROM category"); if (mysqli_num_rows($result_categories) > 0) { while($row = mysqli_fetch_assoc($result_categories)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                <?php } } ?>
                              </select>
                            </div>
                            <div class="text-right">
                              <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                              <button class="btn btn-primary" type="submit">Add</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal -->
                  </div>
                </div>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Borrower's Name</th>
                      <th class="py-10">Books Borrowed</th>
                      <th class="py-10">Date Borrowed</th>
                      <th class="py-10">Date Due</th>
                      <th class="py-10" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($conn, "SELECT * FROM loan");
                    if (mysqli_num_rows($result) > 0) { while($row_loans = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_loans['id'];?></td>
                      <td data-toggle="modal" data-target="#loan-edit-modal-<?php echo $row_loans['id'];?>" class="cursor-pointer py-10 px-25 text-left">
                        <?php $result_borrower = mysqli_query($conn, "SELECT * FROM borrower WHERE id=".$row_loans['borrower_id']);
                        if (mysqli_num_rows($result_borrower) > 0) { while($row_borrower = mysqli_fetch_assoc($result_borrower)) {
                          echo $row_borrower['last_name'] . ", " . $row_borrower['first_name'] . " " .$row_borrower['middle_name']; } } ?>
                      </td>
                      <td data-toggle="modal" data-target="#loan-edit-modal-<?php echo $row_loans['id'];?>" class="cursor-pointer py-10 px-25">
                        <?php $result_book = mysqli_query($conn, "SELECT * FROM book WHERE id=".$row_loans['borrower_id']);
                        if (mysqli_num_rows($result_book) > 0) { while($row_book = mysqli_fetch_assoc($result_book)) {
                          echo $row_book['title'] . '<br />by ' . $row_book['author']; } } ?></td>
                      <td class="py-10 px-25"><?php echo date('M d, Y', strtotime($row_loans['date']));?></td>
                      <td class="py-10 px-25"><?php echo date('M d, Y', strtotime($row_loans['date_due']));?></td>
                      <td>
                        <a data-toggle="modal" data-target="#loan-edit-modal-<?php echo $row_loans['id'];?>" href="javascript:void(0)"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="loan-edit-modal-<?php echo $row_loans['id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit <?php echo $row_loans['title'];?></h4>
                              </div>
                              <div class="modal-body">
                                <input type="text" class="d-none" name="edit_loan" value="<?php echo $row_loans['id'];?>">
                                <div class="form-group">
                                  <input type="text" class="form-control" name="edit_loan_title" placeholder="<?php echo $row_loans['title'];?>" value="<?php echo $row_loans['title'];?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="edit_loan_author" placeholder="<?php echo $row_loans['author'];?>" value="<?php echo $row_loans['author'];?>">
                                </div>
                                <div class="form-group">
                                  <select name="edit_loan_category[]" class="form-control" sdata-plugin="select2">
                                    <?php
                                    $result_categories = mysqli_query($conn, "SELECT * FROM category");
                                    if (mysqli_num_rows($result_categories) > 0) { while($row_categories = mysqli_fetch_assoc($result_categories)) { ?>
                                      <?php
                                      $selected = "";
                                      $result_loan_category_link = mysqli_query($conn, "SELECT * FROM loan_category_link WHERE loan_id=".$row_loans['id']);
                                      while($row_loan_category_link = mysqli_fetch_assoc($result_loan_category_link)) {
                                        if ($row_loan_category_link['category_id'] == $row_categories['id']) { $selected = "selected"; } }
                                      ?>
                                      <option value="<?= $row_categories['id'] ?>" <?= $selected ?>><?= $row_categories['name'] ?></option>
                                    <?php } } ?>
                                  </select>
                                </div>
                                <div class="text-right">
                                  <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                                  <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_loan" value="<?php echo $row_loans['id'];?>">
                          <input type="text" class="d-none" name="delete_loan_name" value="<?php echo $row_loans['title'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>