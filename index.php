<?php include("header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-content blue-grey-500">

        <!-- Add Loan -->
        <?php
        if (isset($_POST['add_loan_borrower'])) {
          $error = "";
          $sql = "INSERT INTO loan (borrower_id, book_id, date, date_due) VALUES ('" . 
            mysqli_real_escape_string( $conn , $_POST['add_loan_borrower'] ) . "', '" .
            mysqli_real_escape_string( $conn , $_POST['add_loan_book'] ) . "', '" .
            date('Y-m-d H:i:s', strtotime($_POST['add_loan_date'])) . "', '" .
            date('Y-m-d H:i:s', strtotime($_POST['add_loan_date_due'])) . "')";
          if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
          if ( $error !== "" ) { ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            ERROR : <?php echo $error; ?>
          </div>
          <?php } else { ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            SUCCESS : Loan has been added. <a href="<?php echo $root_dir; ?>/loans">View all book loans.</a>
          </div>
        <?php } } ?>

        <!-- Add Borrower -->
        <?php
        if (isset($_POST['add_borrower_first_name'])) {
          $sql = "INSERT INTO borrower (first_name, middle_name, last_name, address, contact_number) VALUES ('" . 
            mysqli_real_escape_string( $conn , $_POST['add_borrower_first_name'] ) . "', '" .
            mysqli_real_escape_string( $conn , $_POST['add_borrower_middle_name'] ) . "', '" .
            mysqli_real_escape_string( $conn , $_POST['add_borrower_last_name'] ) . "', '" .
            mysqli_real_escape_string( $conn , $_POST['add_borrower_address'] ) . "', '" .
            mysqli_real_escape_string( $conn , $_POST['add_borrower_contact_number'] ) . "')";
          if ( !(mysqli_query($conn, $sql)) ) { ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            ERROR : <?php echo mysqli_error($conn); ?>
          </div>
          <?php } else { ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            SUCCESS : Borrower <strong><?php echo $_POST['add_borrower_first_name']; ?></strong> has been added. <a href="<?php echo $root_dir; ?>/borrowers">View all book borrowers.</a>
          </div>
        <?php } } ?>

        <!-- Add Book -->
        <?php
        if (isset($_POST['add_book_title'])) {
          $error = "";
          $sql = "INSERT INTO book (title, author) VALUES ('" . 
            mysqli_real_escape_string( $conn , $_POST['add_book_title'] ) . "', '" .
            mysqli_real_escape_string( $conn , $_POST['add_book_author'] ) . "')";
          if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
          // Add categories to book
          $sql = "";
          foreach ($_POST['add_book_category'] as $key => $value)
            $sql .= "(" . mysqli_insert_id($conn) . ", " . $value . "), ";
          if ($sql !== "") {
            $sql = "INSERT INTO book_category_link (book_id, category_id) VALUES " . $sql;
            $sql = substr( $sql, 0, (strlen($sql) - 2) );
            if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
          }
          if ( $error !== "" ) { ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            ERROR : <?php echo $error; ?>
          </div>
          <?php } else { ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            SUCCESS : Book <strong><?php echo $_POST['add_book_title']; ?></strong> has been added. <a href="<?php echo $root_dir; ?>/inventory">View all books.</a>
          </div>
        <?php } } ?>

        <!-- Add Category -->
        <?php
        if (isset($_POST['add_category'])) {
          $error = "";
          $sql = "INSERT INTO category (name) VALUES ('" . $_POST['add_category'] . "')";
          if ( !(mysqli_query($conn, $sql)) ) { ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            ERROR : <?php echo mysqli_error($conn); ?>
          </div>
          <?php } else { ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            SUCCESS : Book category <strong><?php echo $_POST['add_category']; ?></strong> has been added. <a href="<?php echo $root_dir; ?>/category">View all book categories.</a>
          </div>
          <?php }
          ?>
        <?php } ?>
        
        <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-3 blocks-md-2" data-plugin="masonry">

          <li class="masonry-item">
            <div class="card card-shadow">
              <div class="card-block">
                <h4 class="card-title">Loan Slip</h4>
                <form action="" method="post">
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Borrower</label>
                    </div>
                    <div class="col-md-8">
                      <select name="add_loan_borrower" class="form-control" data-plugin="select2">
                        <?php $result_borrower = mysqli_query($conn, "SELECT * FROM borrower"); if (mysqli_num_rows($result_borrower) > 0) { while($row_borrowers = mysqli_fetch_assoc($result_borrower)) { ?>
                        <option value="<?= $row_borrowers['id'] ?>"><?= $row_borrowers['last_name'] . ", " . $row_borrowers['first_name'] . " " . $row_borrowers['middle_name'] ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Book</label>
                    </div>
                    <div class="col-md-8">
                      <select name="add_loan_book" class="form-control" data-plugin="select2">
                        <?php $result_book = mysqli_query($conn, "SELECT * FROM book"); if (mysqli_num_rows($result_book) > 0) { while($row_books = mysqli_fetch_assoc($result_book)) { ?>
                        <option value="<?= $row_books['id'] ?>"><strong><?= $row_books['title'] ?></strong> by <?= $row_books['author'] ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Loan Date</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" id="inputDateOfBirth" class="form-control" data-plugin="datepicker" name="add_loan_date" value="<?php echo date('m/d/Y'); ?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Date Due</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" id="inputDateOfBirth" class="form-control" data-plugin="datepicker" name="add_loan_date_due" value="<?php echo date("m/d/Y", strtotime("+7 day")); ?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="text-right">
                    <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </li>

          <li class="masonry-item">
            <div class="card card-shadow">
              <div class="card-block">
                <h4 class="card-title">Return Slip</h4>
                <form action="" method="post">
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Borrower</label>
                    </div>
                    <div class="col-md-8">
                      <select name="add_loan_borrower" class="form-control" data-plugin="select2">
                        <?php $result_borrower = mysqli_query($conn, "SELECT * FROM borrower"); if (mysqli_num_rows($result_borrower) > 0) { while($row_borrowers = mysqli_fetch_assoc($result_borrower)) { ?>
                        <option value="<?= $row_borrowers['id'] ?>"><?= $row_borrowers['last_name'] . ", " . $row_borrowers['first_name'] . " " . $row_borrowers['middle_name'] ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Book</label>
                    </div>
                    <div class="col-md-8">
                      <select name="add_loan_book" class="form-control" data-plugin="select2">
                        <?php $result_book = mysqli_query($conn, "SELECT * FROM book"); if (mysqli_num_rows($result_book) > 0) { while($row_books = mysqli_fetch_assoc($result_book)) { ?>
                        <option value="<?= $row_books['id'] ?>"><strong><?= $row_books['title'] ?></strong> by <?= $row_books['author'] ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Loan Date</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" id="inputDateOfBirth" class="form-control" data-plugin="datepicker" name="birth_date" value="<?php echo date('m/d/Y'); ?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label class="form-control-label">Date Due</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" id="inputDateOfBirth" class="form-control" data-plugin="datepicker" name="birth_date" value="<?php echo date("m/d/Y", strtotime("+7 day")); ?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="text-right">
                    <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </li>

          <li class="masonry-item">
            <div class="card card-shadow">
              <div class="card-block">
                <h4 class="mb-25">Add a Borrower</h4>
                <form action="" method="post">
                  <div class="row text-left">
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_borrower_first_name" placeholder="First Name" required>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_borrower_middle_name" placeholder="Middle Name">
                    </div>
                    <div class="col-md-7 form-group">
                      <input type="text" class="form-control" name="add_borrower_last_name" placeholder="Last Name" required>
                    </div>
                    <div class="col-md-5 form-group">
                      <input type="text" class="form-control" name="add_borrower_email" placeholder="Contact No">
                    </div>
                    <div class="col-md-12 form-group">
                      <input type="text" class="form-control" name="add_borrower_contact_number" placeholder="Address">
                    </div>
                    <div class="col-md-12 text-right">
                      <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                      <button class="btn btn-primary" type="submit">Add</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </li>

          <li class="masonry-item">
            <div class="card card-shadow">
              <div class="card-block">
                <h4 class="card-title">Add a Book</h4>
                <form action="" method="post">
                  <div class="form-group">
                    <input type="text" class="form-control" name="add_book_title" placeholder="Title">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="add_book_author" placeholder="Author">
                  </div>
                  <div class="form-group">
                    <select name="add_book_category[]" class="form-control" sdata-plugin="select2">
                      <?php $result_categories = mysqli_query($conn, "SELECT * FROM category"); if (mysqli_num_rows($result_categories) > 0) { while($row = mysqli_fetch_assoc($result_categories)) { ?>
                      <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                      <?php } } ?>
                    </select>
                  </div>
                  <div class="text-right">
                    <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                    <button class="btn btn-primary" type="submit">Add</button>
                  </div>
                </form>
              </div>
            </div>
          </li>

          <li class="masonry-item">
            <div class="card card-shadow">
              <div class="card-block">
                <h4 class="card-title">Add a Book Category</h4>
                <form action="" method="post">
                  <div class="input-group">
                    <input type="text" name="add_category" class="form-control py-20" placeholder="Category Name">
                    <button type="submit" class="btn btn-primary input-group-addon">Add Category</button>
                  </div>
                </form>
              </div>
            </div>
          </li>

        </ul>
      </div>
    </div>

<?php include("footer.php"); ?>