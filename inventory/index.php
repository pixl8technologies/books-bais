<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Books Inventory</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Books Inventory</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-12">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Book -->
                <?php
                if (isset($_POST['add_book_title'])) {
                  $error = "";
                  $sql = "INSERT INTO book (title, author) VALUES ('" . 
                    mysqli_real_escape_string( $conn , $_POST['add_book_title'] ) . "', '" .
                    mysqli_real_escape_string( $conn , $_POST['add_book_author'] ) . "')";
                  if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
                  // Add categories to book
                  $sql = "";
                  foreach ($_POST['add_book_category'] as $key => $value)
                    $sql .= "(" . mysqli_insert_id($conn) . ", " . $value . "), ";
                  if ($sql !== "") {
                    $sql = "INSERT INTO book_category_link (book_id, category_id) VALUES " . $sql;
                    $sql = substr( $sql, 0, (strlen($sql) - 2) );
                    if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
                  }
                  if ( $error !== "" ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo $error; ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Book <strong><?php echo $_POST['add_book_title']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <!-- Edit book -->
                <?php
                if (isset($_POST['edit_book'])) {
                  $error = "";
                  $sql = "UPDATE book SET " .
                    "title='" . $_POST['edit_book_title'] . "', " .
                    "author='" . $_POST['edit_book_author'] . "' WHERE id=" . $_POST['edit_book'];
                  if ( !(mysqli_query($conn, $sql)) ) $error .= mysqli_error($conn);
                  if ( $error !== "" ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo $error; ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Book <strong><?php echo $_POST['edit_book_title']; ?></strong> (ID = <?php echo $_POST['edit_book']; ?>) data has been updated.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Delete book -->
                <?php
                if (isset($_POST['delete_book'])) {
                  $error = "";
                  $sql = "DELETE FROM book_category_link WHERE book_id=" . $_POST['delete_book'];
                  if ( !(mysqli_query($conn, $sql)) ) $error .= "";
                  $sql = "DELETE FROM book WHERE id=" . $_POST['delete_book'];
                  if ( !(mysqli_query($conn, $sql)) ) $error .= "";
                  if ( $error !== "" ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo $error; ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Book <strong><?php echo $_POST['delete_book_name']; ?></strong> (ID = <?php echo $_POST['delete_book']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <div class="position-relative">
                  <h4 class="mb-20 pt-10">All Books</h4>
                  <div class="position-absolute" style="right: 0; top: 0;">
                    <button data-toggle="modal" data-target="#add-book-modal" class="btn btn-primary" type="button">Add a Book</button>
                    <!-- Modal -->
                    <div class="modal fade modal-fade-in-scale-up" id="add-book-modal" aria-hidden="false" role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <form class="modal-content" action="" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Add a Book</h4>
                          </div>
                          <div class="modal-body">
                            <div class="form-group">
                              <input type="text" class="form-control" name="add_book_title" placeholder="Title" required>
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="add_book_author" placeholder="Author" required>
                            </div>
                            <div class="form-group">
                              <select name="add_book_category[]" class="form-control" sdata-plugin="select2">
                                <?php $result_categories = mysqli_query($conn, "SELECT * FROM category"); if (mysqli_num_rows($result_categories) > 0) { while($row = mysqli_fetch_assoc($result_categories)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                <?php } } ?>
                              </select>
                            </div>
                            <div class="text-right">
                              <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                              <button class="btn btn-primary" type="submit">Add Book</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal -->
                  </div>
                </div>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Title</th>
                      <th class="py-10">Author</th>
                      <th class="py-10">Categories</th>
                      <th class="py-10" style="min-width: 65px;">Status</th>
                      <th class="py-10" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($conn, "SELECT * FROM book");
                    if (mysqli_num_rows($result) > 0) { while($row_books = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_books['id'];?></td>
                      <td data-toggle="modal" data-target="#book-edit-modal-<?php echo $row_books['id'];?>" class="cursor-pointer py-10 px-25 text-left"><?php echo $row_books['title'];?></td>
                      <td data-toggle="modal" data-target="#book-edit-modal-<?php echo $row_books['id'];?>" class="cursor-pointer py-10 px-25"><?php echo $row_books['author'];?></td>
                      <td class="py-10 px-25">
                        <?php
                        $result_category_of_books = mysqli_query($conn, "SELECT * FROM book_category_link WHERE book_id=" . $row_books['id']);
                        if (mysqli_num_rows($result_category_of_books) > 0) { while($row_category_of_books = mysqli_fetch_assoc($result_category_of_books)) { ?>
                        <span class="badge badge-outline badge-default">
                          <?php $result_cob = mysqli_query($conn, "SELECT name FROM category WHERE id=" . $row_category_of_books['category_id']);
                          if (mysqli_num_rows($result_cob) > 0) $row_cob = mysqli_fetch_assoc($result_cob); echo $row_cob['name']; ?>
                        </span>
                        <?php } } ?>
                      </td>
                      <td class="py-10 px-25"><?php echo ($row_books['loan_id'] === null | $row_books['loan_id'] === "") ?
                        '<span class="badge badge-outline badge-default">On hand</span>' :
                        '<span class="badge badge-outline badge-info">Borrowed</span>' ;?></td>
                      <td>
                        <a data-toggle="modal" data-target="#book-edit-modal-<?php echo $row_books['id'];?>" href="javascript:void(0)"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="book-edit-modal-<?php echo $row_books['id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit <?php echo $row_books['title'];?></h4>
                              </div>
                              <div class="modal-body">
                                <input type="text" class="d-none" name="edit_book" value="<?php echo $row_books['id'];?>">
                                <div class="form-group">
                                  <input type="text" class="form-control" name="edit_book_title" placeholder="<?php echo $row_books['title'];?>" value="<?php echo $row_books['title'];?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="edit_book_author" placeholder="<?php echo $row_books['author'];?>" value="<?php echo $row_books['author'];?>">
                                </div>
                                <div class="form-group">
                                  <select name="edit_book_category[]" class="form-control" sdata-plugin="select2">
                                    <?php
                                    $result_categories = mysqli_query($conn, "SELECT * FROM category");
                                    if (mysqli_num_rows($result_categories) > 0) { while($row_categories = mysqli_fetch_assoc($result_categories)) { ?>
                                      <?php
                                      $selected = "";
                                      $result_book_category_link = mysqli_query($conn, "SELECT * FROM book_category_link WHERE book_id=".$row_books['id']);
                                      while($row_book_category_link = mysqli_fetch_assoc($result_book_category_link)) {
                                        if ($row_book_category_link['category_id'] == $row_categories['id']) { $selected = "selected"; } }
                                      ?>
                                      <option value="<?= $row_categories['id'] ?>" <?= $selected ?>><?= $row_categories['name'] ?></option>
                                    <?php } } ?>
                                  </select>
                                </div>
                                <div class="text-right">
                                  <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                  <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_book" value="<?php echo $row_books['id'];?>">
                          <input type="text" class="d-none" name="delete_book_name" value="<?php echo $row_books['title'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>