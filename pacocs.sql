INSERT INTO book (title, author) VALUES
  ('Alexander and the Terrible, Horrible, No Good, Very Bad Day', 'Judith Viorst'), 
  ('Bark, George', 'Jules Feiffer'), 
  ('A Chair For My Mother', 'Vera B. Williams'), 
  ('D''Aulaires'' Book of Greek Myths', 'Ingri D''Aulaire and Edgar Parin D''Aulaire');

INSERT INTO category (name) VALUES
  ('Technical'), 
  ('Novel'), 
  ('Fictional'), 
  ('Encyclopedia');

INSERT INTO book_category_link (book_id, category_id) VALUES
  (1, 2), 
  -- (1, 3), 
  (2, 1), 
  -- (2, 2), 
  -- (2, 3), 
  (3, 4);

INSERT INTO borrower(first_name, last_name, address, contact_number) VALUES
  ('Amy','Santiago','4 Address','0987654321'),
  ('Jake','Peralta','4 Address','0987654321'),
  ('Rosa','Diaz','4 Address','0987654321'),
  ('Terry','Jeffords','4 Address','0987654321'),
  ('Charles','Boyle','4 Address','0987654321'),
  ('Gina','Linetti','4 Address','0987654321'),
  ('Raymond','Holt','4 Address','0987654321');