<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Borrowers</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Borrowers</li>
        </ol>
       <div class="page-header-actions d-none">
        <form class="form-inline d-inline-block mr-10">
        <div class="form-group footable-filtering-search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" style="padding: 18px;">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>
        </form>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-5">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Add Borrower -->
                <?php
                if (isset($_POST['add_borrower_first_name'])) {
                  $sql = "INSERT INTO borrower (first_name, middle_name, last_name, address, contact_number) VALUES ('" . 
                    mysqli_real_escape_string( $conn , $_POST['add_borrower_first_name'] ) . "', '" .
                    mysqli_real_escape_string( $conn , $_POST['add_borrower_middle_name'] ) . "', '" .
                    mysqli_real_escape_string( $conn , $_POST['add_borrower_last_name'] ) . "', '" .
                    mysqli_real_escape_string( $conn , $_POST['add_borrower_address'] ) . "', '" .
                    mysqli_real_escape_string( $conn , $_POST['add_borrower_contact_number'] ) . "')";
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Borrower <strong><?php echo $_POST['add_borrower_first_name']; ?></strong> has been added.
                  </div>
                <?php } } ?>

                <h4 class="mb-25">Add a Borrower in Database</h4>

                <form action="" method="post">
                  <div class="row text-left">
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_borrower_first_name" placeholder="First Name" required>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_borrower_middle_name" placeholder="Middle Name">
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_borrower_last_name" placeholder="Last Name" required>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="add_borrower_contact_number" placeholder="Contact Number">
                    </div>
                    <div class="col-md-12 form-group">
                      <input type="text" class="form-control" name="add_borrower_address" placeholder="Address">
                    </div>
                    <div class="col-md-12 text-right">
                      <button class="btn btn-default mr-10" data-dismiss="modal" type="reset">Reset</button>
                      <button class="btn btn-primary" type="submit">Add</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

          <div class="col-md-7">
            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show">

                <!-- Edit borrower -->
                <?php
                if (isset($_POST['edit_borrower'])) {
                  $sql = "UPDATE borrower SET " .
                    "first_name='" .  mysqli_real_escape_string( $conn , $_POST['edit_borrower_first_name'] ) . "', " .
                    "middle_name='" . mysqli_real_escape_string( $conn ,$_POST['edit_borrower_middle_name'] ) . "', " .
                    "last_name='" . mysqli_real_escape_string( $conn ,$_POST['edit_borrower_last_name'] ) . "', " .
                    "contact_number='" . mysqli_real_escape_string( $conn ,$_POST['edit_borrower_contact_number'] ) . "', " .
                    "address='" . mysqli_real_escape_string( $conn ,$_POST['edit_borrower_address'] ) . "' WHERE id=" . $_POST['edit_borrower'];
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Borrower <strong><?php echo $_POST['edit_borrower_first_name']; ?></strong> (ID = <?php echo $_POST['edit_borrower']; ?>) data has been updated.
                  </div>
                  <?php }
                  ?>
                <?php } ?>

                <!-- Delete borrower -->
                <?php
                if (isset($_POST['delete_borrower'])) {
                  $sql = "DELETE FROM borrower WHERE id=" . $_POST['delete_borrower'];
                  if ( !(mysqli_query($conn, $sql)) ) { ?>
                  <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ERROR : <?php echo mysqli_error($conn); ?>
                  </div>
                  <?php } else { ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    SUCCESS : Borrower <strong><?php echo $_POST['delete_borrower_name']; ?></strong> (ID = <?php echo $_POST['delete_borrower']; ?>) has been deleted.
                  </div>
                <?php } } ?>

                <h4 class="mb-25">Borrowers in Database</h4>

                <table class="table table-hover table-bordered table-striped toggle-arrow-tiny table-sm text-center" id="borrower-delete-table">
                  <thead>
                    <tr>
                      <th class="py-10">ID</th>
                      <th class="py-10">Name</th>
                      <th class="py-10">Contact No</th>
                      <th class="py-10">Loans</th>
                      <th class="py-10">Fine</th>
                      <th class="py-10" style="min-width: 65px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $result = mysqli_query($conn, "SELECT * FROM borrower ORDER BY last_name");
                    if (mysqli_num_rows($result) > 0) { while($row_borrowers = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?php echo $row_borrowers['id'];?></td>
                      <td data-toggle="modal" data-target="#borrower-edit-modal-<?php echo $row_borrowers['id'];?>" class="cursor-pointer py-10 px-25 text-left"><?php echo $row_borrowers['last_name'];?>, <?php echo $row_borrowers['first_name'];?> <?php echo $row_borrowers['middle_name'];?></td>
                      <td data-toggle="modal" data-target="#borrower-edit-modal-<?php echo $row_borrowers['id'];?>" class="cursor-pointer py-10 px-25"><?php echo $row_borrowers['contact_number'];?></td>
                      <td class="py-10 px-25"><?php echo $row_borrowers['on_loan'];?></td>
                      <td class="py-10 px-25"><?php echo $row_borrowers['overdue_fine'];?></td>
                      <td>
                        <a data-toggle="modal" data-target="#borrower-edit-modal-<?php echo $row_borrowers['id'];?>" href="javascript:void(0)"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <!-- Modal -->
                        <div class="modal fade modal-fade-in-scale-up" id="borrower-edit-modal-<?php echo $row_borrowers['id'];?>" aria-hidden="false" role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <form class="modal-content" action="" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Borrower Data</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row text-left">
                                  <input type="text" class="d-none" name="edit_borrower" value="<?php echo $row_borrowers['id'];?>">
                                  <div class="col-md-6 form-group">
                                    <input type="text" class="form-control" name="edit_borrower_first_name" placeholder="First Name" value="<?php echo $row_borrowers['first_name'];?>" required>
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <input type="text" class="form-control" name="edit_borrower_middle_name" placeholder="Middle Name" value="<?php echo $row_borrowers['middle_name'];?>">
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <input type="text" class="form-control" name="edit_borrower_last_name" placeholder="Last Name" value="<?php echo $row_borrowers['last_name'];?>" required>
                                  </div>
                                  <div class="col-md-6 form-group">
                                    <input type="text" class="form-control" name="edit_borrower_contact_number" placeholder="Contact Number" value="<?php echo $row_borrowers['contact_number'];?>">
                                  </div>
                                  <div class="col-md-12 form-group">
                                    <input type="text" class="form-control" name="edit_borrower_address" placeholder="Address" value="<?php echo $row_borrowers['address'];?>">
                                  </div>
                                  <div class="col-md-12 text-right">
                                    <button class="btn btn-default mr-10" data-dismiss="modal" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Save</button>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <form action="" method="post" class="form-button" >
                          <input type="text" class="d-none" name="delete_borrower" value="<?php echo $row_borrowers['id'];?>">
                          <input type="text" class="d-none" name="delete_borrower_name" value="<?php echo $row_borrowers['first_name'];?>">
                          <button type="submit"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true"></i></button>
                        </form>
                      </td>
                    </tr>
                    <?php } } else { ?>
                    <tr><td colspan="999" class="text-center">No records.</td></tr>
                    <?php } ?>
                  </tbody>
                </table>

              </div>
            </div>
            <!-- End Panel Accordion -->
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>