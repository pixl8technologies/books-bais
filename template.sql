CREATE TABLE IF NOT EXISTS `book` (
    `id` integer primary key auto_increment,
    `title` varchar(200),
    `author` varchar(200),
    `loan_id` integer
);

CREATE TABLE IF NOT EXISTS `category` (
    `id` integer primary key auto_increment,
    `name` varchar(35)
);

-- For Book-Category many-to-many cardinality
CREATE TABLE IF NOT EXISTS `book_category_link` (
    `id` integer primary key auto_increment,
    `book_id` integer,
    `category_id` integer
);

CREATE TABLE IF NOT EXISTS `borrower` (
    `id` integer primary key auto_increment,
    `first_name` varchar(35),
    `middle_name` varchar(35),
    `last_name` varchar(35),
    `address` varchar(100),
    `contact_number` varchar(100),
    `on_loan` integer,
    `overdue_fine` integer
);

CREATE TABLE IF NOT EXISTS `hand_back` (
    `id` integer primary key auto_increment,
    `date` date
);

CREATE TABLE IF NOT EXISTS `loan` (
    `id` integer primary key auto_increment,
    `date` date,
    `date_due` date,
    `borrower_id` integer,
    `hand_back_id` integer,
    `book_id` integer
);

-- For Loan-Return many-to-many cardinality
CREATE TABLE IF NOT EXISTS `loan_hand_back_link` (
    `id` integer primary key auto_increment,
    `loan_id` integer,
    `hand_back_id` integer
);

CREATE TABLE IF NOT EXISTS `user` (
    `id` integer primary key auto_increment,
    `username` varchar(35),
    `password` varchar(128)
);

INSERT INTO `user` (`id`, `username`, `password`) VALUES ('1', 'admin', 'password');

-- Foreign keys
ALTER TABLE `book` ADD FOREIGN KEY (`loan_id`) REFERENCES loan(`id`);

ALTER TABLE `book_category_link` ADD FOREIGN KEY (`book_id`) REFERENCES book(`id`);
ALTER TABLE `book_category_link` ADD FOREIGN KEY (`category_id`) REFERENCES category(`id`);

ALTER TABLE `loan` ADD FOREIGN KEY (`borrower_id`) REFERENCES borrower(`id`);
ALTER TABLE `loan` ADD FOREIGN KEY (`hand_back_id`) REFERENCES hand_back(`id`);
ALTER TABLE `loan` ADD FOREIGN KEY (`book_id`) REFERENCES book(`id`);

ALTER TABLE `loan_hand_back_link` ADD FOREIGN KEY (`loan_id`) REFERENCES loan(`id`);
ALTER TABLE `loan_hand_back_link` ADD FOREIGN KEY (`hand_back_id`) REFERENCES hand_back(`id`);